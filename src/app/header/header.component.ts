import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private titleService: Title) { }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

  ngOnInit() {

    var main=function(){$('.dropdown').click(function(){$(this).fadeOut()})};$(document).ready(main);(function($){$('div.whatsappme__button').hover(function(){$('div.whatsappme__box').show()});$('div.whatsappme__close').click(function(){$('div.whatsappme__box').hide()})}(jQuery))
  

}
}
