import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeepamtalentComponent } from './deepamtalent.component';

describe('DeepamtalentComponent', () => {
  let component: DeepamtalentComponent;
  let fixture: ComponentFixture<DeepamtalentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeepamtalentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeepamtalentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
