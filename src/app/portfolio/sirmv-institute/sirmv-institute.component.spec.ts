import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SirmvInstituteComponent } from './sirmv-institute.component';

describe('SirmvInstituteComponent', () => {
  let component: SirmvInstituteComponent;
  let fixture: ComponentFixture<SirmvInstituteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SirmvInstituteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SirmvInstituteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
