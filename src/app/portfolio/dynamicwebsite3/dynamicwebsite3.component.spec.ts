import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dynamicwebsite3Component } from './dynamicwebsite3.component';

describe('Dynamicwebsite3Component', () => {
  let component: Dynamicwebsite3Component;
  let fixture: ComponentFixture<Dynamicwebsite3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dynamicwebsite3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dynamicwebsite3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
