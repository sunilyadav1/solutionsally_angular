import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SjpchillerservicesComponent } from './sjpchillerservices.component';

describe('SjpchillerservicesComponent', () => {
  let component: SjpchillerservicesComponent;
  let fixture: ComponentFixture<SjpchillerservicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SjpchillerservicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SjpchillerservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
