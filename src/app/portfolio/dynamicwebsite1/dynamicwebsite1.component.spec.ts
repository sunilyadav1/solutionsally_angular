import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dynamicwebsite1Component } from './dynamicwebsite1.component';

describe('Dynamicwebsite1Component', () => {
  let component: Dynamicwebsite1Component;
  let fixture: ComponentFixture<Dynamicwebsite1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dynamicwebsite1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dynamicwebsite1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
