import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpmasterpestcontrolComponent } from './npmasterpestcontrol.component';

describe('NpmasterpestcontrolComponent', () => {
  let component: NpmasterpestcontrolComponent;
  let fixture: ComponentFixture<NpmasterpestcontrolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpmasterpestcontrolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpmasterpestcontrolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
