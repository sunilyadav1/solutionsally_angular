import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ecommerce3Component } from './ecommerce3.component';

describe('Ecommerce3Component', () => {
  let component: Ecommerce3Component;
  let fixture: ComponentFixture<Ecommerce3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ecommerce3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ecommerce3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
