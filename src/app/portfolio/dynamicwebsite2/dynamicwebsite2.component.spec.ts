import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dynamicwebsite2Component } from './dynamicwebsite2.component';

describe('Dynamicwebsite2Component', () => {
  let component: Dynamicwebsite2Component;
  let fixture: ComponentFixture<Dynamicwebsite2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dynamicwebsite2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dynamicwebsite2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
