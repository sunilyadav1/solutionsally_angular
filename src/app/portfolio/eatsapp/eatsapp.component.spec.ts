import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EatsappComponent } from './eatsapp.component';

describe('EatsappComponent', () => {
  let component: EatsappComponent;
  let fixture: ComponentFixture<EatsappComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EatsappComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EatsappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
