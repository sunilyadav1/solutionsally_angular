import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ecommerce4Component } from './ecommerce4.component';

describe('Ecommerce4Component', () => {
  let component: Ecommerce4Component;
  let fixture: ComponentFixture<Ecommerce4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ecommerce4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ecommerce4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
