import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dynamicwebsite4Component } from './dynamicwebsite4.component';

describe('Dynamicwebsite4Component', () => {
  let component: Dynamicwebsite4Component;
  let fixture: ComponentFixture<Dynamicwebsite4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dynamicwebsite4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dynamicwebsite4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
