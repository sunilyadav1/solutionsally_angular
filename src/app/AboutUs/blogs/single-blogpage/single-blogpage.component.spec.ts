import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBlogpageComponent } from './single-blogpage.component';

describe('SingleBlogpageComponent', () => {
  let component: SingleBlogpageComponent;
  let fixture: ComponentFixture<SingleBlogpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleBlogpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBlogpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
