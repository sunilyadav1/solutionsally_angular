import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import { ToasterService } from './../toaster-service.service';
import { HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule,  FormGroup,  FormControl,  Validators,  FormBuilder} from '@angular/forms';
declare var  $: any;
declare var jQuery: any;
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})

export class ContactUsComponent implements OnInit {

form = new FormGroup({
  name: new FormControl('', Validators.required),
  //email: new FormControl('', Validators.required);
  email: new FormControl('', [
    Validators.required,
    Validators.pattern("[^ @]*@[^ @]*")
  ]),
  subject:  new FormControl('', Validators.required),
  message:  new FormControl('', Validators.required),

});



// get name(){
//   return this.form.get('name');
// }


onSubmit(){
  if( this.form.value.name!="" && this.form.value.email!="" ){
     console.log("submitted");
     this.form.reset();
  }
 
  // if(this.form)
}



  showMessage : boolean = true;
  submitted: string;

  name: string = "";
  email: string = "";
  subject: string = "";
  message: string = "";

  constructor(public router: Router, public _loginService: LoginService, private toasterService: ToasterService) { }
  Success(){this.toasterService.Success("Your message has been sent successfully.")}
Info(){this.toasterService.Info("Subscribed")}

ngOnInit(){
  
  $('#customers-carousel3').owlCarousel({responsive:{0:{items:1},600:{items:3},1000:{items:4}},loop:true,margin:30,nav:true,smartSpeed:2000,navText:["<i class='fa fa-chevron-left position'></i>","<i class='fa fa-chevron-right position1'></i>"],autoplay:true,autoplayTimeout:3000});
  
  
  (function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    

})(jQuery);
}

login() {
  console.log();
  let user = new User('', '', '', '');
  user.name = this.form.value.name;
  user.email = this.form.value.email;
  user.subject = this.form.value.subject;
  user.message = this.form.value.message;
  let data = [{
      'name': user.name,
      'email': user.email,
      'subject': user.subject,
      'message': user.message
  }];

  console.log(data);
  
  if( this.form.value.name!="" && this.form.value.email!="" && this.form.value.subject!="" && this.form.value.name!=null && this.form.value.email!=null && this.form.value.subject!=null  ){
  this._loginService.sendLogin({
      data
  }).subscribe(
    response => this.handleResponse(response),
    error => this.handleResponse(error),
     );
 this.showMessage = false;
  this.Success();
 this.form.reset();
      
}
}
handleResponse(response) {
  if (response.success) {
      console.log("success")
  } else if (response.error) {
      console.log("errror")
  } else {}
}

}
