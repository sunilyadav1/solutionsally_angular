import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RarDevelopmentComponent } from './rar-development.component';

describe('RarDevelopmentComponent', () => {
  let component: RarDevelopmentComponent;
  let fixture: ComponentFixture<RarDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RarDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RarDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
