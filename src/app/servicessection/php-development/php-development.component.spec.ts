import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhpDevelopmentComponent } from './php-development.component';

describe('PhpDevelopmentComponent', () => {
  let component: PhpDevelopmentComponent;
  let fixture: ComponentFixture<PhpDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhpDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhpDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
